package main

import (
	"context"
	"net/http"
	"os"

	ddlambda "github.com/DataDog/datadog-lambda-go"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/rs/zerolog/log"
	gtmHpStorage "gitlab.com/gtmotorsports/house-points-storage-plugin-gtm"
	hp "gitlab.com/johnrichter/house-points"
	hpeventconsumer "gitlab.com/johnrichter/house-points-worker/consumer"
	hpeventbus "gitlab.com/johnrichter/house-points/event/bus"
	"gitlab.com/johnrichter/tracing-go"
	traceext "gopkg.in/DataDog/dd-trace-go.v1/ddtrace/ext"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
)

func init() {
	// Trace all http calls
	http.DefaultClient = tracing.DefaultHTTPClient

	env := os.Getenv("DD_ENV")
	service := os.Getenv("DD_SERVICE")
	version := os.Getenv("DD_VERSION")
	log.Logger = log.With().Fields(
		map[string]interface{}{
			"env":     env,
			"service": service,
			"version": version,
		},
	).Logger()
}

func main() {
	lambda.Start(ddlambda.WrapHandler(handleSQSEvent, nil))
}

func handleSQSEvent(ctx context.Context, e *events.SQSEvent) error {
	span, ok := tracer.SpanFromContext(ctx)
	if !ok {
		log.Error().Msg("Unable to get current span from context")
	}
	span.SetOperationName("aws.sqs.event")
	span.SetTag(traceext.ResourceName, "aws.sqs.event")

	// Connect our traces and logs
	log.Logger = log.With().
		Uint64(tracing.LogDDTraceIDKey, span.Context().TraceID()).
		Uint64(tracing.LogDDSpanIDKey, span.Context().SpanID()).
		Logger()

	lctx, ok := lambdacontext.FromContext(ctx)
	if !ok {
		log.Error().Msg("Lambda context malformed or missing")
		return &ErrMalformedLambdaContext
	}
	config, err := NewWorkerConfig(ctx, lctx)
	if err != nil {
		log.Error().Err(err).Msg("Unable to create worker config")
		return &ErrConfiguration
	}
	gtmHpStorage.TraceGtmWebRequests(config.GtmWebStorageService)
	hpStores := []hp.HousePointsStoragePlugin{
		gtmHpStorage.NewGtmWebStorage(config.HousePointsSubmissionURL),
	}
	p := hpeventconsumer.NewHousePointsEventConsumer(hpStores)
	eventBus := hpeventbus.NewSQSEventBus(
		&config.EventBusConfig,
		&config.SqsEventBusConfig,
		config.sqs,
		p,
		config.SlackVerificationToken,
	)
	eventBus.ProcessMessages(ctx, e.Records)
	return nil
}

package main

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"strings"
	"sync"

	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/aws/aws-sdk-go-v2/aws"
	awsConfig "github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/kms"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	hp "gitlab.com/johnrichter/house-points"
	hpeventbus "gitlab.com/johnrichter/house-points/event/bus"
	slackapp "gitlab.com/johnrichter/slack-app"
	"gitlab.com/johnrichter/tracing-go"
)

const (
	ProductionEnvironment = "production"
)

type WorkerConfig struct {
	hp.HousePointsConfig         `mapstructure:",squash"`
	hpeventbus.EventBusConfig    `mapstructure:",squash"`
	hpeventbus.SqsEventBusConfig `mapstructure:",squash"`
	slackapp.SlackConfig         `mapstructure:",squash"`
	SlackConfigData              string `json:"slack_config_data" mapstructure:"slack_config_data"`
	Environment                  string `json:"environment" mapstructure:"environment"`
	GtmWebStorageService         string `json:"gtm_web_storage_service" mapstructure:"gtm_web_storage_service"`
	awsConfig                    *aws.Config
	kms                          *kms.Client
	sqs                          *sqs.Client
	ctx                          context.Context
	lctx                         *lambdacontext.LambdaContext
}

func NewWorkerConfig(ctx context.Context, lctx *lambdacontext.LambdaContext) (*WorkerConfig, error) {
	v := viper.New()
	v.SetConfigType("json")
	v.SetEnvPrefix("hp")
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	v.SetDefault("environment", ProductionEnvironment)
	v.SetDefault("gtm_web_storage_service", "")
	v.SetDefault("slack_config_data", "")
	v.AutomaticEnv()

	ac, err := awsConfig.LoadDefaultConfig(
		ctx,
		awsConfig.WithHTTPClient(tracing.AwsHttpClient),
		awsConfig.WithAPIOptions([]tracing.AwsApiOption{tracing.AddAwsXrayTraceHeadersMiddleware}),
	)
	if err != nil {
		return nil, err
	}
	rc := WorkerConfig{
		ctx:       ctx,
		lctx:      lctx,
		awsConfig: &ac,
		kms:       kms.NewFromConfig(ac),
		sqs:       sqs.NewFromConfig(ac),
	}
	// This is a hack. Viper needs defaults before it'll load from the env
	hpc := hp.NewDefaultHousePointsConfig()
	cb, err := json.Marshal(hpc)
	if err != nil {
		return nil, err
	}
	if err = v.MergeConfig(bytes.NewReader(cb)); err != nil {
		return nil, err
	}
	sac := slackapp.NewDefaultSlackConfig()
	cb, err = json.Marshal(sac)
	if err != nil {
		return nil, err
	}
	if err = v.MergeConfig(bytes.NewReader(cb)); err != nil {
		return nil, err
	}
	ebc := hpeventbus.NewDefaultEventBusConfig()
	cb, err = json.Marshal(ebc)
	if err != nil {
		return nil, err
	}
	if err = v.MergeConfig(bytes.NewReader(cb)); err != nil {
		return nil, err
	}
	sebc := hpeventbus.NewDefaultSqsEventBusConfig()
	cb, err = json.Marshal(sebc)
	if err != nil {
		return nil, err
	}
	if err = v.MergeConfig(bytes.NewReader(cb)); err != nil {
		return nil, err
	}
	// Load settings from env
	if err = v.Unmarshal(&rc); err != nil {
		return nil, err
	}
	if rc.isProduction() {
		if err = rc.decryptSecrets(); err != nil {
			return nil, err
		}
	}
	if rc.SlackConfigData != "" {
		var sac slackapp.SlackConfig
		if err := json.Unmarshal(json.RawMessage(rc.SlackConfigData), &sac); err != nil {
			log.Error().Err(err).Msg("Unable to unmarshal Slack App Config json data")
			return nil, err
		}
		rc.SlackConfig = sac
	}
	return &rc, nil
}

func (c *WorkerConfig) isProduction() bool {
	return c.Environment == ProductionEnvironment
}

func (c *WorkerConfig) decryptSecrets() error {
	var wg sync.WaitGroup
	wg.Add(1)
	go c.DecryptKMSCiphertext(&wg, &c.SlackConfigData)
	wg.Wait()
	if len(c.SlackConfigData) == 0 {
		return &ErrDecryptSecrets
	}
	return nil
}

func (c *WorkerConfig) DecryptKMSCiphertext(wg *sync.WaitGroup, ciphertext *string) {
	defer wg.Done()
	emptyText := ""
	blob, err := base64.StdEncoding.DecodeString(*ciphertext)
	if err != nil {
		*ciphertext = emptyText
		log.Error().Err(err).Msg("Unable to base64 decode a KMS encrypted config variable")
		return
	}
	input := &kms.DecryptInput{
		CiphertextBlob: blob,
		EncryptionContext: map[string]string{
			"LambdaFunctionName": lambdacontext.FunctionName,
		},
	}
	result, err := c.kms.Decrypt(c.ctx, input)
	if err != nil {
		log.Error().Err(err).Msg("Unable to KMS decrypt a config variable")
		*ciphertext = emptyText
	} else {
		decryptedText := string(result.Plaintext)
		*ciphertext = decryptedText
	}
}

package main

var (
	ErrMalformedLambdaContext = HousePointsWorkerError{Message: "Lambda context malformed or missing"}
	ErrConfiguration          = HousePointsWorkerError{Message: "Unable to create worker config"}
	ErrDecryptSecrets         = HousePointsWorkerError{Message: "Unable to decrypt configuration secrets"}
	ErrUnmarshal              = HousePointsWorkerError{Message: "Could not unmarshal JSON payload"}
)

type HousePointsWorkerError struct {
	Message string `json:"message"`
}

func (e *HousePointsWorkerError) Error() string {
	return e.Message
}
